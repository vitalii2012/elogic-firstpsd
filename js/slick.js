$(document).ready(()=> {
    $('.your-slider').slick({
        autoplay: false,
        arrows: true,
        infinite: true,
        slidesToShow: 3.9,
        slidesToScroll: 1,
        centerMode: false,
        mobileFirst:true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3.9,
                    // slidesToScroll: 1,
                    arrows: true,

                }
            },
            {
                breakpoint: 968,
                settings: {
                    slidesToShow: 3.5,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            },
            {
                breakpoint: 750,
                settings: {
                    slidesToShow: 3,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            },
            {
                breakpoint: 650,
                settings: {
                    slidesToShow: 2.7,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 2.2,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            },
            {
                breakpoint: 450,
                settings: {
                    slidesToShow: 2,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            },
            {
                breakpoint: 350,
                settings: {
                    slidesToShow: 1.5,
                    // slidesToScroll: 1,
                    arrows: true,
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});